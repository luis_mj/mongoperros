@extends('layouts.app')

@section('content')

@if(Auth::user() !== NULL)
@if(Auth::user()->isAdmin())
  @php
    $admin = true
  @endphp
@else
  @php
    $admin = false
  @endphp
@endif
@else
  @php
    $admin = false
  @endphp
@endif

<h1>Facturas</h1>
<div class="container">
  <div class="row">
    <div class="col-md">
        <section class="content">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="float-left">
                  <h3>facturas</h3>
                </div>
                <div class="float-right">
                  <div class="btn-group">
                    <a href="{{ route('factura.create') }}" class="btn btn-info" >Crear Factura</a>
                  </div>
                </div>
                  <div class="table-container">
                    <table id="mytable" class="table table-bordered table-striped">
                       <thead>
                         <th>ID</th>
                         <th>Fecha</th>
                         <th>Total</th>
                         <th>Metodo Pago</th>
                         <th>Detalle</th>
                       </thead>
                        <tbody>
                          @if($facturas->count())
                          @foreach($facturas as $factura)  
                          <tr>
                            <td>{{ $factura->id }}</td>
                            <td> {{ $factura->fecha->toDateTime()->format('d-m-Y H:i:s ') }} </td>
                            <td>{{ '$'.$factura->total }}</td>
                            @if($factura->metodo_pago==1)
                            <td>Efectivo</td>
                            @endif
                            @if($factura->metodo_pago==2)
                            <td>Débito</td>
                            @endif
                            @if($factura->metodo_pago==3)
                            <td>Crédito</td>
                            @endif
                            <td>
                           	 <a class="btn btn-primary btn-xs" href="{{ action('facturaController@show', $factura->id) }}" >
                                Detalle
                             </a>
                           	</td>
                           </tr>
                           
                           @endforeach 
                           @else
                          	<tr>
                            	<td colspan="8">No hay facturas registrados</td>
                          	</tr>
                          @endif
                        </tbody>
                    </table>
                  </div>
              </div>
              {{ $facturas->render() }}
            </div>
          </div>
        </section>
    </div>
</div>
</div>


@endsection