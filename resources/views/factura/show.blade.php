@extends('layouts.app')

@section('content')
<h1>Detalles de Factura</h1>
@php
 $total = 0
@endphp
<section class="content">
	<div class="container">
		<div class="row">
		<div class="col-md"></div>
		<div class="col-md-6">
			<div class="row">
				<label><strong>ID: {{ $factura->id }} </strong></label>
			</div>
			<div class="row">
				<label><strong>Fecha: {{ $factura->fecha->toDateTime()->format('d-m-Y H:i:s ') }}</strong> </label>
			</div>
			<div class="row">
				<div class="panel panel-default">
	              	<div class="panel-body">
	                	<div class="float-left">
	                  		<h3>Perros</h3>
	                	</div>
	                	<div class="table-container">
	                		<table id="mytable" class="table table-bordered table-striped">
	                			<thead>
	                				<th>Nombre</th>
	                				<th>Precio Unitario</th>
	                				<th>Cantidad</th>
	                				<th>Precio Neto</th>
	                			</thead>
	                			<tbody>
	                				@if(!empty($factura->perros))
									@foreach($factura->perros as $perro)
										<tr>
											<td>{{$perro['nombre']}}</td>
											<td>{{'$'.$perro['precio']}}</td>
											<td>{{$perro['cantidad']}}</td>
											<td>{{'$'.$perro['precio'] * $perro['cantidad']}}</td>
										</tr>
										@php
											$total += $perro['precio'] * $perro['cantidad']
										@endphp
									@endforeach
									@else
									 	<tr>
                            				<td colspan="8">No hay perros registrados</td>
                          				</tr>
									@endif
	                			</tbody>
	                		</table>
	                	</div>
	            	</div>
				</div>
			</div>

			<div class="row">
				<div class="panel panel-default">
	              	<div class="panel-body">
	                	<div class="float-left">
	                  		<h3>Refrescos</h3>
	                	</div>
	                	<div class="table-container">
	                		<table id="mytable" class="table table-bordered table-striped">
	                			<thead>
	                				<th>Nombre</th>
	                				<th>Precio Unitario</th>
	                				<th>Cantidad</th>
	                				<th>Precio Neto</th>
	                			</thead>
	                			<tbody>
	                				@if(!empty($factura->refrescos))
									@foreach($factura->refrescos as $refresco)
										<tr>
											<td>{{$refresco['nombre']}}</td>
											<td>{{'$'.$refresco['precio']}}</td>
											<td>{{$refresco['cantidad']}}</td>
											<td>{{'$'.$refresco['precio'] * $refresco['cantidad']}}</td>
										</tr>
										@php
											$total += $refresco['precio'] * $refresco['cantidad']
										@endphp
									@endforeach
									@else
									 	<tr>
                            				<td colspan="8">No hay refrescos registrados</td>
                          				</tr>
									@endif
	                			</tbody>
	                		</table>
	                	</div>
	            	</div>
				</div>
			</div>



			<div class="row float-left">
				<a href="{{ route('factura.index') }}" class="btn btn-info">Atras</a>
			</div>
			<div class="row float-right">
				
				<label><strong>TOTAL: {{'$'.$total}}</strong></label>
			</div>
		</div>
		<div class="col-md"></div>

		</div>	
	</div>
	
</section>



@endsection