@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10">
		<label>Perro:</label>
		<select id="dropdownList">
		@foreach($data['perros'] as $perro)
			<option value='{{$perro->id}}' class = 'postPerro'> {{$perro->nombre}}</option>
		@endforeach
		</select>
		<label>Precio:</label>
		<label class="preciop"></label>
		<label>Cantidad:</label>
		<input type="text" id="cantidadP" name="cantidadP" value="1">
		<button id="addPerro" class="btn btn-info">+</button>
		</div>
		<div class="col-md-2"></div>
	</div>
	
	<div class="row">
		<div class="col-md-10">
		<label>Refresco:</label>
		<select id="dropdownList2">
		@foreach($data['refrescos'] as $refresco)
			<option value="{{$refresco->id}}" class = "postRefresco">{{$refresco->nombre}}</option>
		@endforeach
		</select>
		<label>Precio:</label>
		<label class="precior"></label>
		<label>Cantidad: </label>
		<input type="text" id="cantidadR" name="cantidadR" value="1">
		<button id="addRefresco" class="btn btn-info">+</button>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<label>Método de pago:</label>
			<select id="dropdownListMetodos">
				<option value="1">Efectivo</option>
				<option value="2">Débito</option>
				<option value="3">Crédito</option>
			</select>
		</div>
		<div class="col-md-4">
			<button id="facturaButton" class="btn btn-success">Facturar</button>
			<a class="btn btn-warning" href="{{ route('factura.index') }}">Regresar</a>
			<strong>TOTAL: $<label id="total"></label></strong>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="planel-body">
					<table id="myTable" class="table table-bordered table-striped">
						<thead>
							<th>ID Perro</th>
							<th>Nombre Perro</th>
							<th>Cantidad</th>
							<th>Precio Unitario</th>
							<th>Total</th>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="planel-body">
					<table id="refrescoTable" class="table table-bordered table-striped">
						<thead>
							<th>ID Refresco</th>
							<th>Nombre Refresco</th>
							<th>Cantidad</th>
							<th>Precio Unitario</th>
							<th>Total</th>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</div>

<script type="text/javascript">

$(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var _total = 0;

	$('#dropdownList').on('change',function(){
	        //var optionValue = $(this).val();
	        //var optionText = $('#dropdownList option[value="'+optionValue+'"]').text();
	        /*var optionText = $("#dropdownList option:selected").val();
	        alert("Selected Option Text: "+optionText);*/
	       	$.ajax({
            url: '/perroF',
            type: 'POST',
            data: {_token: CSRF_TOKEN, message: $("#dropdownList option:selected").val()},
            dataType: 'JSON',

            success: function (data) {
                $(".preciop").text(data.msg); 
            }
        	}); 
	});

	$('#dropdownList2').on('change',function(){
        $.ajax({
            url: '/refrescoF',
            type: 'POST',
            data: {_token: CSRF_TOKEN, message: $("#dropdownList2 option:selected").val()},
            dataType: 'JSON',

            success: function (data) {
                $(".precior").text(data.msg); 
            }
        }); 
	});
    $.ajax({
            url: '/perroF',
            type: 'POST',
            data: {_token: CSRF_TOKEN, message: $("#dropdownList option:selected").val()},
            dataType: 'JSON',

            success: function (data) {
                $(".preciop").text(data.msg); 
            }
    }); 
    $.ajax({
            url: '/refrescoF',
            type: 'POST',
            data: {_token: CSRF_TOKEN, message: $("#dropdownList2 option:selected").val()},
            dataType: 'JSON',

            success: function (data) {
                $(".precior").text(data.msg); 
            }
    });

    $('#addPerro').click(function(){
 		$('#myTable').append('<tr ><td>'+$("#dropdownList option:selected").val()+'</td><td id="attrName">'+$("#dropdownList option:selected").text() +'</td><td id="attrQty">'+$("#cantidadP").val()+'</td><td id="attrValue">'+$(".preciop").text()+'</td><td>'+($(".preciop").text()*$("#cantidadP").val())+'</td></tr>');
 		_total += ($(".preciop").text()*$("#cantidadP").val());
 		$('#total').text(_total);
    });

      $('#addRefresco').click(function(){

 		$('#refrescoTable').append('<tr><td>'+$("#dropdownList2 option:selected").val()+'</td><td id="attrName">'+$("#dropdownList2 option:selected").text() +'</td><td id="attrQty">'+$("#cantidadR").val()+'</td><td id="attrValue">'+$(".precior").text()+'</td><td>'+($(".precior").text()*$("#cantidadR").val())+'</td></tr>');
 		_total += ($(".precior").text()*$("#cantidadR").val());
 		$('#total').text(_total);
    });

    var perros = new Array();
    var refrescos = new Array();

    $('#facturaButton').click(function(){
    	_perros = [];
            $('#myTable tr').each(function (i, el) {
            	var $tds = $(this).find('td'),
            	id = $tds.eq(0).text(),
                name = $tds.eq(1).text(),
                cant = $tds.eq(2).text(),
                value = $tds.eq(3).text();
                _perros.push({ _id: id, nombre: name, precio: value, cantidad: cant });

            });
            _perros.shift();

        _refrescos = [];
            $('#refrescoTable tr').each(function (i, el) {
            	var $tds = $(this).find('td'),
            	id = $tds.eq(0).text(),
                name = $tds.eq(1).text(),
                cant = $tds.eq(2).text(),
                value = $tds.eq(3).text();
                _refrescos.push({ _id : id, nombre: name, precio: value, cantidad: cant });

            });
        	_refrescos.shift();
        	var metodo = $("#dropdownListMetodos option:selected").val();

        	
        	var datos = {perros: null,refrescos: null,fecha: null,total: null, metodo_pago: null};
        	datos['perros'] = _perros;
        	datos['refrescos'] = _refrescos;
        	datos['total'] = _total;
        	datos['metodo_pago'] = metodo;

        	var _mensaje = JSON.stringify(datos);

        	$.ajax({
            url: '/facturaGuardar',
            type: 'POST',
            dataType: 'JSON',
            data: {_token: CSRF_TOKEN, message: _mensaje },
            

            success: function (data) {
            	window.location.replace("/factura");
            }
    });


	});

});  


</script>

@endsection
