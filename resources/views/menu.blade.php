@extends('layouts.app')
@section('content')

@if(Auth::user() !== NULL)
@if(Auth::user()->isAdmin())
  @php
    $admin = true
  @endphp
@else
  @php
    $admin = false
  @endphp
@endif
@else
  @php
    $admin = false
  @endphp
@endif
<h1>Menu</h1>
<div class="container">
  <div class="row">
    <div class="col-md">
        <section class="content">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="float-left">
                  <h3>Perros</h3>
                </div>
                @if($admin)
                <div class="float-right">
                  <div class="btn-group">
                    <a href="{{ route('perro.create') }}" class="btn btn-info" >Añadir Perro</a>
                  </div>
                </div>
                @endif
                  <div class="table-container">
                    <table id="mytable" class="table table-bordered table-striped">
                       <thead>
                         <th>Nombre</th>
                         <th>Precio</th>
                       </thead>
                        <tbody>
                          @if($data['perros']->count())  
                          @foreach($data['perros'] as $perro)  
                          <tr>
                            <td>{{$perro->nombre}}</td>
                            <td>{{$perro->precio}}</td>
                            @if($admin)
                            <td>
                              <a class="btn btn-primary btn-xs" href="{{ action('perroController@edit', $perro->id)}}" >
                                Editar
                              </a>
                            </td>
                            <td>
                              <form action="{{ action('perroController@destroy',$perro->id)}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-xs" type="submit">
                                  Eliminar
                                </button>
                              </form>
                            </td>
                            @endif
                           </tr>
                           @endforeach 
                           @else
                           <tr>
                            <td colspan="8">No hay perros registrados</td>
                          </tr>
                          @endif
                        </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </section>
    </div>


    <div class="col-md">
      <section class="content">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="float-left"><h3>Refrescos</h3></div>
              @if($admin)
              <div class="float-right">
                <div class="btn-group">
                  <a href="{{ route('refresco.create') }}" class="btn btn-info" >Añadir Refresco</a>
                </div>
              </div>
              @endif
                <div class="table-container">
                  <table id="mytable" class="table table-bordered table-striped">
                     <thead>
                       <th>Nombre</th>
                       <th>Precio</th>
                      <tbody>
                        @if($data['refrescos']->count())  
                        @foreach($data['refrescos'] as $refresco)  
                        <tr>
                          <td>{{$refresco->nombre}}</td>
                          <td>{{$refresco->precio}}</td>
                          @if($admin)
                          <td>
                              <a class="btn btn-primary btn-xs" href="{{ action('refrescoController@edit', $refresco->id)}}" >
                                Editar
                              </a>
                            </td>
                            <td>
                              <form action="{{ action('refrescoController@destroy', $refresco->id) }}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-xs" type="submit">
                                  Eliminar
                                </button>
                              </form>
                            </td>
                          @endif
                         </tr>
                         @endforeach 
                         @else
                         <tr>
                          <td colspan="8">No hay refrescos registrados</td>
                        </tr>
                        @endif
                      </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection