@extends('layouts.app')
@section('content')

@if(Auth::user() !== NULL)
@if(Auth::user()->isAdmin())
  @php
    $admin = true
  @endphp
@else
  @php
    $admin = false
  @endphp
@endif
@else
  @php
    $admin = false
  @endphp
@endif
<h1>Menu</h1>
<div class="container">
  <div class="row">
    <div class="col-md">
        <section class="content">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="float-left">
                  <h3>Facturas</h3>
                </div>
                
                  <div class="table-container">
                    <table id="mytable" class="table table-bordered table-striped">
                       <thead>
                         <th>ID</th>
                         <th>Total ventas</th>
                         <th>Total ingresos</th>
                        
                       </thead>
                        <tbody>
                          @if($reporteMensual['factura']->count())  
                          @foreach($reporteMensual['factura'] as $factura)  
                          <tr>
                            <td>{{$factura->_id}}</td>
                            <td>{{$factura->total_ventas}}</td>
                            <td>{{$factura->total_ingresos}}</td>
                        
                           </tr>
                           @endforeach 
                           @else
                           <tr>
                            <td colspan="8">No hay facturas registrados</td>
                          </tr>
                          @endif
                        </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </section>
    </div>

@endsection