@extends('layouts.app')
@section('content')

@if(Auth::user() !== NULL)
@if(Auth::user()->isAdmin())
  @php
    $admin = true
  @endphp
@else
  @php
    $admin = false
  @endphp
@endif
@else
  @php
    $admin = false
  @endphp
@endif
<h1>Menu</h1>
<div class="container">
  <div class="row">
    <div class="col-md">
        <section class="content">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="float-left">
                  <h3>Ventas del dia</h3>
                </div>
                
                  <div class="table-container">
                    <table id="mytable" class="table table-bordered table-striped">
                       <thead>
                         <th>Fecha</th>
                         <th>Total ingresos</th>
                        
                       </thead>
                        <tbody>
                          @if($reporteDiario->count())  
                          @foreach($reporteDiario as $factura)  
                          <tr>
                            <td>{{$factura->fecha}}</td>
            z
                            <td>{{$factura->total}}</td>
                        
                           </tr>
                           @endforeach 
                           @else
                           <tr>
                            <td colspan="8">No hay facturas registrados</td>
                          </tr>
                          @endif
                        </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </section>
    </div>

@endsection