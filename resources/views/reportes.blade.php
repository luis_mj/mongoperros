@extends('layouts.app')
@section('content')

@if(Auth::user() !== NULL)
@if(Auth::user()->isAdmin())
  @php
    $admin = true
  @endphp
@else
  @php
    $admin = false
  @endphp
@endif
@else
  @php
    $admin = false
  @endphp
@endif
<h1>Reportes</h1>
<div class="container">
  <div class="row">
    <div class="col-md">
        <section class="content">
          <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="float-left">
                  <a class="navbar-brand" href="{{ url('/ganancias') }}">
                    Ganancias por mes
                </a>
        
                <a class="navbar-brand" href="{{ url('/tipoPerro') }}">
                   Tipos de perro pedidos
                </a>
                <a class="navbar-brand" href="{{ url('/tipoRefresco') }}">
                  Tipos de refresco pedidos
               </a>
                </div>
                
                
        </section>
    </div>
</div>
</div>
@endsection