@extends('layouts.app')
@section('content')
	<section class="content">
		<div class="container">
		<div class="row">
		<div class="col-md"></div>
		<div class="col-md-6">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>¡Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
 
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Editar Refresco</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" action="{{ route('refresco.update',$refresco->id) }}"  role="form">
							{{ csrf_field() }}
							<input name="_method" type="hidden" value="PATCH">
							<div class="row">
								<div class="col-md-2">
									Nombre:
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<input type="text" name="nombre" id="nombre" class="form-control input-sm" value="{{$refresco->nombre}}">
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-2">
									Precio:
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<input type="text" name="precio" id="precio" class="form-control input-sm" value="{{$refresco->precio}}">
									</div>
								</div>

							</div>
							<div class="row">
									<div class="col-md-6">
										<input type="submit"  value="Actualizar" class="btn btn-success btn-block">
									</div>
									<div class="col-md-6">
										<a href="{{ route('menu') }}" class="btn btn-info btn-block" >Atrás</a>	
									</div>
							</div>
											
								
						</form>
					</div>
				</div>
 
			</div>
		</div>
		<div class="col-md"></div>
	</div>
	</div>
	</section>

@endsection