<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return redirect()->route('menu');
});

Route::get('/menu','MenuController@index')->name('menu');
Route::post('/perroF','facturaController@perroF');
Route::post('/refrescoF','facturaController@refrescoF');
Route::post('/facturaGuardar','facturaController@facturaGuardar');
Route::resource('refresco','refrescoController');
Route::resource('perro','perroController');
Route::resource('factura','facturaController');
Route::get('/reportes', 'MenuController@reportes')->name('reportes');
Route::get('/ganancias', 'MenuController@ganancias')->name('ganancias');
Route::get('/tipoPerro', 'MenuController@tipoPerro')->name('tipoPerro');
Route::get('/tipoRefresco', 'MenuController@tipoRefresco')->name('tipoRefresco');
Route::get('/reporteDiario', 'MenuController@reporteDiario')->name('reporteDiario');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post', 'PostController@form')->name('post.form');
Route::get('/post/save', 'PostController@save')->name('post.save');

