<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(users_collection_seeder::class);
        $this->call(perro_collection_seeder::class);
        $this->call(refresco_collection_seeder::class);
        $this->call(factura_collection_seeder::class);
    }
}
