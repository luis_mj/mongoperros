<?php

use Illuminate\Database\Seeder;

class factura_collection_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Factura::class,200)->create();
    }
}
