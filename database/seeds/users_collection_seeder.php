<?php

use Illuminate\Database\Seeder;

class users_collection_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\User::class,10)->create();

    	DB::table('users_collection')->insert([        
    		'name' => 'cosa1',
	        'email' => 'cosa1@mail.com',
	        /*'email_verified_at' => now(),*/
	        
	        'password' => bcrypt('foo'),
	        'role' => 1,
	       /* 'remember_token' => Str::random(10)*/
	    ]);

    	DB::table('users_collection')->insert([        
    		'name' => 'cosa2',
	        'email' => 'cosa2@mail.com',
	        /*'email_verified_at' => now(),*/
	        
	        'password' => bcrypt('foo'), // password
	        /*'remember_token' => Str::random(10)*/
	        'role' => 2
	    ]);
    }
}
