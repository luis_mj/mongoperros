<?php

use Illuminate\Database\Seeder;

class refresco_collection_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Refresco::class,10)->create();
    }
}
