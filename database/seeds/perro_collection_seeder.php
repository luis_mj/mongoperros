<?php

use Illuminate\Database\Seeder;

class perro_collection_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Perro::class,8)->create();
    }
}
