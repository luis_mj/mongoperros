<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Perro;
use Faker\Generator as Faker;

$factory->define(Perro::class, function (Faker $faker) {
    return [
    	'nombre' => $faker->lastName,
    	'precio' => $faker->randomFloat(2,2000,5000)
    ];
});
