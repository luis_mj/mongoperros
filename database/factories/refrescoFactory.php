<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Refresco;
use Faker\Generator as Faker;

$factory->define(Refresco::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name.' cola',
        'precio' => $faker->randomFloat(2,500,3000) 
    ];
});
