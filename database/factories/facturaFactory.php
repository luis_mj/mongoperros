<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Factura;
use Faker\Generator as Faker;

$factory->define(Factura::class, function (Faker $faker) {
	$perros = DB::table('perro_collection')->take(2)->get();
	$refrescos = DB::table('refresco_collection')->take(3)->get();
    $total = 0;
    $newPerros = array();
    foreach ($perros as $innerArray) {
        $innerArray['cantidad'] = $faker->numberBetween(1,5);
        $newPerros[] = $innerArray;
        $total += $innerArray['precio']*$innerArray['cantidad'];
    }
    $perros = $newPerros;
    $newRefrescos = array();
    foreach ($refrescos as $innerArray) {
        $innerArray['cantidad'] = $faker->numberBetween(1,5);
        $newRefrescos[] = $innerArray;
        $total += $innerArray['precio']*$innerArray['cantidad'];    
    }
    $refrescos = $newRefrescos;

    return [
        'perros' => $perros,
        'refrescos' => $refrescos,
        'fecha' => $faker->dateTimeBetween('-2 months','now',null),
        'total' => $total,
        'metodo_pago' => $faker->numberBetween(1,3)
    ]; 
});
