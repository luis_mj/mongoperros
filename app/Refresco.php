<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Refresco extends Eloquent
{
	protected $connection = 'mongodb';

    protected $collection = 'refresco_collection';
    protected $fillable = ['nombre','precio'];
}
