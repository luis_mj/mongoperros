<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Perro extends Eloquent
{
	protected $connection = 'mongodb';

 

    protected $collection = 'perro_collection';
    protected $fillable = ['nombre','precio','cantidad'];
}
