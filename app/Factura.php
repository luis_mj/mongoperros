<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Factura extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'factura_collection';
    protected $dates = ['fecha'];
    protected $fillable = ['perros','refrescos','fecha','total','metodo_pago'];
}
