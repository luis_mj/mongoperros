<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factura;
use App\Perro;
use App\Refresco;

class facturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('factura.index', ['facturas' => Factura::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['perros'] = Perro::all();
        $data['refrescos'] = Refresco::all();
        return view('factura.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Factura::create($request->all());
        return redirect()->route('factura.index')->with('success','¡Factura registrada de manera exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factura = Factura::find($id);
        return view('factura.show',compact('factura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $factura = Factura::find($id);
        return view('factura.edit',compact('factura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validations
        Factura::find($id)->update($request->all());
        return redirect()->route('factura.index')->with('success','¡Se ha actualizado el registro de factura de manera exitosa!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Factura::find($id)->delete();
        return redirect()->route('factura.index')->with('success','¡Se ha borrado el registro de factura de manera exitosa!');
    }

    public function perroF(Request $request){

        $perro = Perro::find($request->message);
        $response = array(
          'status' => 'success',
          'msg' => $perro->precio,
        );
      return response()->json($response);
    }

    public function refrescoF(Request $request){

        $refresco = Refresco::find($request->message);
        $response = array(
          'status' => 'success',
          'msg' => $refresco->precio,
        );
      return response()->json($response);
    }

    public function facturaGuardar(Request $request){

        $rawJson = $request->message;
        $decoded = json_decode($rawJson,true);
        $factura = new Factura();
        //$factura = forceFill($decoded);
        //$factura->save();
        $factura['perros'] = $decoded['perros'];
        $factura['fecha'] = now();
        $factura['refrescos'] = $decoded['refrescos'];
        $factura['total'] = $decoded['total'];
        $factura['metodo_pago'] = $decoded['metodo_pago'];

        $factura->save();

        $response = array(
          'status' => 'success',
          'msg' => $factura['metodo_pago'],
        );
        

        return response()->json($response);
    }
}
