<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perro;
use App\Refresco;
use App\Factura;
use Carbon\Carbon;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function reporteDiario(){

		$dia = Carbon::now()->hour(0)->minute(0)->second(0);
		$diaDespues = $dia->copy()->addDay();
		
        $reporteDiario = Factura::where([
			['fecha', '>=', $dia],
			['fecha', '<', $diaDespues],
		
		])->orderBy('fecha','desc')->get();

		$total = $reporteDiario->reduce(function($total,$reporteDiario){
            return $total + $reporteDiario->total;
		});
		
        return view('reporteDiario',['reporteDiario'=>$reporteDiario,'total'=>$total]);

        }


     public function ganancias(){



        $reporteMensual['factura'] = Factura::raw(function ($collection){
			return $collection->aggregate([
				[
					'$addFields' => [
						'mes' => ['$month' => '$fecha'],
					],
				],
			
				[
					'$group' => [
						'_id' => '$mes',
						'total_ventas' => ['$sum' => 1],
						'total_ingresos' => ['$sum' => '$total'],
					],
				],
				[
					'$sort' => [
						'_id' => 1,
					],
				],
			]);
		});

        return view('ganancias',compact('reporteMensual'));

        }

		public function tipoPerro(){

			$tipoPerro['factura'] = Factura::raw(function ($collection){
				return $collection->aggregate([
					
					[
						'$unwind' => '$perros',
					],
					[
						'$group' => [
							'_id' => '$perros.nombre',
							'total_ventas' => ['$sum' => 1],
						
						],
					],
					[
						'$sort' => [
							'_id' => 1,
						],
					],
				]);
			});
	
			return view('tipoPerro',compact('tipoPerro'));
	
			}

			public function tipoRefresco(){

				$tipoRefresco['factura'] = Factura::raw(function ($collection){
					return $collection->aggregate([
						
						[
							'$unwind' => '$refrescos',
						],
						[
							'$group' => [
								'_id' => '$refrescos.nombre',
								'total_ventas' => ['$sum' => 1],
							
							],
						],
						[
							'$sort' => [
								'_id' => 1,
							],
						],
					]);
				});
		
				return view('tipoRefresco',compact('tipoRefresco'));
		
				}

	

    public function index()
    {
        $data['perros'] = Perro::orderBy('nombre','asc')->get();
        $data['refrescos'] = Refresco::orderBy('nombre','asc')->get();
        return view('menu',compact('data'));
	}
	
	public function reportes(){

		return view('reportes');

	}

	/*		public function ingredientes(){

					$ingredientes['factura'] = Factura::raw(function ($collection){
						return $collection->aggregate([
							[
								'$unwind' => '$perros',
							],
						
							[
								'$group' => [
									'_id' => '$perros.numero_ingredientes',
									'total_ingredientes' => ['$sum' => 1],
								
								],
							],
							[
								'$sort' => [
									'_id' => 1,
								],
							],
						]);
					});
			
					return view('ingredientes',compact('ingredientes'));
			
					}*/

}
