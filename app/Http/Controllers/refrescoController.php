<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Refresco;

class refrescoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('refresco.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['nombre'=> 'required', 'precio' => 'required|numeric']);
        Refresco::create($request->all());
        return redirect()->route('menu')->with('success','¡Refresco añadido de manera exitosa!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $refresco = Refresco::find($id);
        return view('refresco.edit',compact('refresco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['nombre'=> 'required', 'precio' => 'required|numeric'],['required'=>'El campo :attribute es obligatorio','numeric' => 'El campo :attribute debe ser un número']);
        Refresco::find($id)->update($request->all());
        return redirect()->route('menu')->with('success','¡Registro de refresco actualizado de manera exitosa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Refresco::find($id)->delete();
        return redirect()->route('menu')->with('success','¡Registro eliminado de manera exitosa!');
    }
}
