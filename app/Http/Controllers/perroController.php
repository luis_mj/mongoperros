<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perro;

class perroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perro.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['nombre'=> 'required', 'precio' => 'required|numeric'],['required'=>'El campo :attribute es obligatorio','numeric' => 'El campo :attribute debe ser un número']);
        Perro::create($request->all());
        return redirect()->route('menu')->with('success','¡Perro añadido de manera exitosa!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perro = Perro::find($id);
        return view('perro.edit',compact('perro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['nombre'=> 'required', 'precio' => 'required|numeric']);
        Perro::find($id)->update($request->all());
        return redirect()->route('menu')->with('success','¡Perro editado de manera exitosa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Perro::find($id)->delete();
        return redirect()->route('menu')->with('success','¡Registro eliminado de manera exitosa!');
    }
}
